This is a small app that converts a MIDI channel's stream into keypresses for use with the playable piano in Garry's Mod. Works on any server that has the playable piano mod installed. 

# Quick guide to get started:

1: Download LoopBe1 from https://www.nerds.de/en/loopbe1.html and install it.

2: Download Aria Maestosa from http://ariamaestosa.sourceforge.net/ and install it.

3: Make sure that LoopBe1 is installed and running (it should show up in your system tray), and launch Aria.

4: Import your MIDI file into Aria.

5: In the top bar of Aria, click Output > LoopBe Internal MIDI v.... 

6: Launch the UI of the piano player, and hit "Start". (REQUIRES JAVAFX!)

7: Sit in the Piano in Garry's Mod, and then press Control to show advanced mode.

8: Set the "Start" value in Aria Maestosa to 0 and press play, and then quickly tab into Gmod.

# Troubleshooting:

Q: Some parts of my song aren't playing!

A: In Aria, go to Settings > Channel Managment > Manual, and then set the channel of every track in the MIDI that you want to play to 0. I'm not sure what's causing this problem, but I'll try to find/fix the problem in a later update.

Q: The window's not responding/not accepting input!

A: The current implementation of the printstream that outputs to the console in the window is a bit of a bodge. It tends to break if a lot of print events happen at once (i.e., you're playing a complex song). Relaunch the program and click the "Disable Console" button. That'll stop printing out the notes played, and should prevent window not responding problems from happening.

Q: The piano's playing a lot of bad notes!

A: 

   Short answer: decrease the tickrate value, but that may not help too much.

   Long answer: The way that the piano works requires the program to delay sharps by a few milliseconds, so that natural notes can be played without being sharped. In some cases when playing chords on a laggy server, or in situations where there's a natural note played briefly after a sharp, the shift key can still technically be pressed, resulting in it playing the natural note as if it were a sharp. Conversely, sometimes a sharp will be played as a natural because the key up instruction for shift may've been called on from a different thread. This shouldn't happen often. but is still possible. There's really no way to make something that works in 100% of cases, but I've gotten this program to function about 99% of the time, which is close enough for most people's uses. You can try editing the midi to work around this problem, or decrease the tickrate.
