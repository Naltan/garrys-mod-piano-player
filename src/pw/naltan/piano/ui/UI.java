package pw.naltan.piano.ui;

import java.io.PrintStream;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import pw.naltan.piano.Player;
import pw.naltan.piano.Preferences;

/**
 * This class creates the entire UI for the program, and manages all of the
 * button presses and whatnot.
 * 
 * Also contains the main method.
 * 
 * @author Nate Stowwe
 *
 */
public class UI extends Application {

	@Override
	public void start(Stage stg) throws Exception {
		try {
			// HBox master, the root of the whole layour
			HBox master = new HBox();
			master.setPadding(new Insets(25, 25, 25, 25));

			// VBox for the config panel on the left side.
			VBox configPanel = new VBox();
			configPanel.setAlignment(Pos.TOP_CENTER);

			// Start button
			ToggleButton buttonStart = new ToggleButton("Start");
			buttonStart.setPadding(new Insets(10, 50, 10, 50));
			buttonStart.setFont(Font.font("Arial", FontWeight.BOLD, 36));
			configPanel.getChildren().add(buttonStart);

			// Config Title
			Label header = new Label(
					"Hover over an option to get its usage.\nChanges take place after (re)starting the player.");
			header.setPadding(new Insets(10, 10, 10, 10));
			header.setTextAlignment(TextAlignment.CENTER);
			header.setAlignment(Pos.CENTER);
			UITools.addTooltip("Just like that!", header);
			configPanel.getChildren().add(header);

			// Configuration Gridpane
			GridPane gp = new GridPane();
			gp.setPadding(new Insets(10, 30, 10, 30));
			gp.setHgap(10);
			gp.setVgap(10);
			configPanel.getChildren().add(gp);

			// Tickrate Label
			Label labelTickrate = new Label("Server Tickrate");
			UITools.addToGP(gp, labelTickrate, 0, 0);

			// Tickrate Text Field
			TextField tFieldTickrate = new TextField("60");
			tFieldTickrate.textProperty().addListener(new ChangeListener<String>() {
				// Make sure that only numbers are entered.
				@Override
				public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
					if (!newValue.matches("\\d*")) {
						tFieldTickrate.setText(newValue.replaceAll("[^\\d]", ""));
					}

				}
			});
			UITools.addToGP(gp, tFieldTickrate, 0, 1);

			// Tickrate Tooltip
			String tickrateTooltip = "The tickrate of the server that you're playing on. If you don't know what that is, leave it at 60.\n\tMINIMUM: 30\n\tMAXIMUM: 120";
			UITools.addTooltip(tickrateTooltip, tFieldTickrate, labelTickrate);

			// Octave Label
			Label labelOctave = new Label("Shift OOB Octaves");
			UITools.addToGP(gp, labelOctave, 1, 0);

			// Octave Checkbox
			CheckBox checkboxOctave = new CheckBox();
			UITools.addToGP(gp, checkboxOctave, 1, 1);

			// Octave Tooltip
			String octaveTooltip = "GMod's piano has an octave range of C2 to C7."
					+ "\n\tCHECKED: any notes that are out of bounds will be octave shifted until they're in-bounds."
					+ "\n\tUNCHECKED: any notes that are out of bounds will not be played.";
			UITools.addTooltip(octaveTooltip, labelOctave, checkboxOctave);

			// Console Label
			Label labelConsole = new Label("Disable Console");
			UITools.addToGP(gp, labelConsole, 2, 0);

			// Console Checkbox
			CheckBox checkboxConsole = new CheckBox();
			UITools.addToGP(gp, checkboxConsole, 2, 1);

			String consoleTooltip = "If your song is especially hectic, you may want to disable printing to the console on the right to save resources."
					+ "\n\tCHECKED: The console doesn't report keypresses."
					+ "\n\tUNCHECKED: The console reports keypresses."
					+ "\nNote that startup/shutdown messages are still reported.";
			UITools.addTooltip(consoleTooltip, labelConsole, checkboxConsole);

			// Final additions
			buttonStart.setOnAction(evt -> {

				// Update settings
				Preferences.SHIFT_OCTAVES = checkboxOctave.isSelected();
				Preferences.USE_CONSOLE = !checkboxConsole.isSelected();
				Preferences.STARTED = buttonStart.isSelected();
				// Tickrate is updated here, it's a bit verbose, but all it does is keep the
				// tickrate within 30-120, and change the background of the box to red if it's
				// invalid and had to be bounded.
				int tickrate = 0;
				try {
					tickrate = Integer.parseInt(tFieldTickrate.getText());
				} catch (NumberFormatException ex) {
				}
				if (tickrate < 30 || tickrate > 120) {
					tFieldTickrate.setStyle("-fx-control-inner-background: #ffaaaa");
					if (tickrate < 30) {
						tickrate = 30;
					} else if (tickrate > 120) {
						tickrate = 120;
					}
				} else {
					tFieldTickrate.setStyle("-fx-control-inner-background: #ffffff");
				}

				// Start the midi capture if start is hit
				if (buttonStart.isSelected()) {
					System.out.println("By Stowwe, https://naltan.pw");
					System.out.println("  Server Tickrate: " + Preferences.TICKRATE + "tps");
					System.out.println("  Shift octaves: " + Preferences.SHIFT_OCTAVES);
					System.out.println("  Output to console: " + Preferences.USE_CONSOLE);
					System.out.println("Capture Started");
					buttonStart.setText("Stop");
				} else {
					System.out.println("\nCapture Stopped\n");
					buttonStart.setText("Start");
				}

			});

			// Console
			TextArea output = new TextArea();
			output.setEditable(false);
			output.setPrefWidth(200);
			output.setPrefHeight(350);
			System.setOut(new PrintStream(new Console(output)));

			// Add all the parts to the master layout
			master.getChildren().add(configPanel);
			master.getChildren().add(output);

			// Set the scene and show it.
			stg.setWidth(600);
			stg.setHeight(400);
			stg.setResizable(false);
			stg.setAlwaysOnTop(true);
			stg.setTitle("GMod Piano Player");
			stg.setOnCloseRequest(evt -> {
				System.exit(1);
			});
			stg.setScene(new Scene(master));
			stg.show();
		} catch (Exception ex) {
			ex.printStackTrace();
			System.exit(-2);
		}
	}

	public static void main(String[] args) {
		Player.initPlayer();
		Application.launch(args);
	}
}
