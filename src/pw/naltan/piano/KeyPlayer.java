package pw.naltan.piano;

/**
 * This class is a thread that plays the GMod piano. The delays are calculated
 * based off of the server tickrate.
 * 
 * @author Nate Stowwe.
 *
 */
public class KeyPlayer extends Thread implements Runnable {

	private byte note;

	public KeyPlayer(byte note) {
		this.note = note;
	}

	private static final int SHIFT = 16;
	private static long DELAY = (long) (1000 / Preferences.TICKRATE * 2);

	public void run() {
		try {

			int k = Player.keys[note].getKeyCode();
			if (Player.keys[note].isSharp()) {

				Thread.sleep(DELAY);
				Player.r.keyPress(SHIFT);

				Thread.sleep(DELAY);
				Player.r.keyPress(k);

				Thread.sleep(DELAY);
				Player.r.keyRelease(SHIFT);

				Thread.sleep(DELAY);
				Player.r.keyRelease(k);
			} else {
				Player.r.keyPress(k);
				Thread.sleep(DELAY);
				Player.r.keyRelease(k);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}