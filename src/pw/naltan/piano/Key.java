package pw.naltan.piano;

/**
 * This class contains all the keycodes and note metadata needed for playing a
 * GMod piano
 * 
 * @author Nate Stowwe
 *
 */
public enum Key {
	// C2 -> C7
	C2(49, false, 2, "C"),
	C2S(49, true, 2, "C"),
	D2(50, false, 2, "D"),
	D2S(50, true, 2, "D"),
	E2(51, false, 2, "E"),
	F2(52, false, 2, "F"),
	F2S(52, true, 2, "F"),
	G2(53, false, 2, "G"),
	G2S(53, true, 2, "G"),
	A2(54, false, 2, "A"),
	A2S(54, true, 2, "A"),
	B2(55, false, 2, "B"),
	C3(56, false, 3, "C"),
	C3S(56, true, 3, "C"),
	D3(57, false, 3, "D"),
	D3S(57, true, 3, "D"),
	E3(48, false, 3, "E"),
	F3(81, false, 3, "F"),
	F3S(81, true, 3, "F"),
	G3(87, false, 3, "G"),
	G3S(87, true, 3, "G"),
	A3(69, false, 3, "A"),
	A3S(69, true, 3, "A"),
	B3(82, false, 3, "B"),
	C4(84, false, 4, "C"),
	C4S(84, true, 4, "C"),
	D4(89, false, 4, "D"),
	D4S(89, true, 4, "D"),
	E4(85, false, 4, "E"),
	F4(73, false, 4, "F"),
	F4S(73, true, 4, "F"),
	G4(79, false, 4, "G"),
	G4S(79, true, 4, "G"),
	A4(80, false, 4, "A"),
	A4S(80, true, 4, "A"),
	B4(65, false, 4, "B"),
	C5(83, false, 5, "C"),
	C5S(83, true, 5, "C"),
	D5(68, false, 5, "D"),
	D5S(68, true, 5, "D"),
	E5(70, false, 5, "E"),
	F5(71, false, 5, "F"),
	F5S(71, true, 5, "F"),
	G5(72, false, 5, "G"),
	G5S(72, true, 5, "G"),
	A5(74, false, 5, "A"),
	A5S(74, true, 5, "A"),
	B5(75, false, 5, "B"),
	C6(76, false, 6, "C"),
	C6S(76, true, 6, "C"),
	D6(90, false, 6, "D"),
	D6S(90, true, 6, "D"),
	E6(88, false, 6, "E"),
	F6(67, false, 6, "F"),
	F6S(67, true, 6, "F"),
	G6(86, false, 6, "G"),
	G6S(86, true, 6, "G"),
	A6(66, false, 6, "A"),
	A6S(66, true, 6, "A"),
	B6(78, false, 6, "B"),
	C7(77, false, 7, "C");
	// holy hell this hurt to make

	private final boolean sharp;
	private final int keyCode, octave;
	private final String name;

	// This block was written for an older version of the program that read midis
	// itself rather than just using the midi channel. I may need this in the
	// future, so I'll keep it here.
	/*
	 * public static final Key[] A_OCTAVE = { A2, A3, A4, A5, A2 };
	 * public static final Key[] B_OCTAVE = { B2, B3, B4, B5, B6 };
	 * public static final Key[] C_OCTAVE = { C2, C3, C4, C5, C6, C7 };
	 * public static final Key[] D_OCTAVE = { D2, D3, D4, D5, D6 };
	 * public static final Key[] E_OCTAVE = { E2, E3, E4, E5, E6 };
	 * public static final Key[] F_OCTAVE = { F2, F3, F4, F5, F6 };
	 * public static final Key[] G_OCTAVE = { G2, G3, G4, G5, G6 };
	 * public static final Key[] AS_OCTAVE = { A2S, A3S, A4S, A5S, A6S, };
	 * public static final Key[] CS_OCTAVE = { C2S, C3S, C4S, C5S, C6S };
	 * public static final Key[] DS_OCTAVE = { D2S, D3S, D4S, D5S, D6S };
	 * public static final Key[] FS_OCTAVE = { F2S, F3S, F4S, F5S, F6S };
	 * public static final Key[] GS_OCTAVE = { G2S, G3S, G4S, G5S, G6S };
	 */

	/**
	 * Initializes a key.
	 * 
	 * @param keyCode - They keycode corresponding to the keyboard key to press
	 * @param sharp   - Whether this key is a sharp or not
	 * @param octave  - The octave that this key is in
	 * @param name    - The name of this key, sans sharp
	 */
	private Key(int keyCode, boolean sharp, int octave, String name) {

		this.keyCode = keyCode;
		this.sharp = sharp;
		this.octave = octave;
		this.name = name;
	}

	/**
	 * Returns true if this note is a sharp, false if not.
	 */
	public boolean isSharp() {

		return this.sharp;
	}

	/**
	 * The keycode corresponding to this note
	 * 
	 * @return The keycode as an integer.
	 */
	public int getKeyCode() {

		return this.keyCode;
	}

	/**
	 * The numeric representation of this note's octave
	 * 
	 * @return The octave
	 */
	public int getOctave() {

		return this.octave;
	}

	/**
	 * The character that represents this note (C, D, E, etc...)<br>
	 * Does not include sharps.
	 * 
	 * @return
	 */
	public String getName() {

		return this.name;
	}

	@Override
	/**
	 * The string representation of this note. This appears as NO(#) where N is the
	 * note's name, O is the note's octave, and (#) will be a sharp if this note has
	 * one.
	 */
	public String toString() {
		String out = this.name + this.octave;
		if (this.sharp) {
			out += "#";
		}
		return out;
	}
}
