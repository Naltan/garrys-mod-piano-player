package pw.naltan.piano;

import java.awt.Robot;
import java.util.ArrayList;
import java.util.List;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Transmitter;

/**
 * This class contains some setup and boilerplate code that captures all midi
 * devices on the system, and initializes an array of keys for easy access
 * during the time when MIDIs are being played.
 * 
 * @author Nate Stowwe
 *
 */
public class Player {

	private static List<MidiDevice> devices = new ArrayList<>();
	static Robot r;
	public static Key[] keys;

	/**
	 * This function initializes the robot, the keys array, and once that's done,
	 * starts capturing the midi device(s).
	 */
	public static void initPlayer() {
		try {
			if (r == null) {
				r = new Robot();
				keys = new Key[133];

				// middle octaves
				for (int i = 36; i < 97; i++) {
					keys[i] = Key.values()[i - 36];
				}

				// lower octaves safeguard, shift by as many as required to get in-bounds
				for (int i = 0; i < 36; i++) {
					keys[i] = Key.values()[i % 12];
				}

				// upper octave safeguard, same idea as lower
				for (int i = 97; i < 133; i++) {
					keys[i] = Key.values()[(61 - 12) + i % 12];
				}
			}

			captureDevice();
		} catch (Exception ex) {
			ex.printStackTrace();
			System.exit(-1);
		}
	}

	/**
	 * This function captures the midi device(s) connected to this system. This will
	 * attempt to do so to every single device it has access to. Maybe in a future
	 * update there should be a picker, but it's not a huge deal right now. How many
	 * conflicting midis is one computer going to even play?
	 */
	private static void captureDevice() {
		MidiDevice device;
		MidiDevice.Info[] infos = MidiSystem.getMidiDeviceInfo();
		for (int i = 0; i < infos.length; i++) {
			try {
				// If possible, get the device and its transmitter, set its receiver to the
				// custom one, and then open the device.
				device = MidiSystem.getMidiDevice(infos[i]);
				Transmitter trans = device.getTransmitter();
				trans.setReceiver(new MidiInputReceiver());
				device.open();
				// if code gets this far without throwing an exception, print a success message
				System.out.println(device.getDeviceInfo() + " Was Opened");
				devices.add(device);
			} catch (MidiUnavailableException e) {
				// We can safely ignore this because all it means is that the device was
				// inaccessible.
			}
		}
	}
}